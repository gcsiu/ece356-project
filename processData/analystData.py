import os
import csv

analystFields = ["analystID", "publisher"]
newAnalystRatingsFields = ["analystID", "headline", "ratingURL", "ratingDate", "ticker"]
analystRatingsFields = ["headline", "url", "date", "stock"]

analystCSV = []
analystRatingsCSV = []
oldFields = []
path = 'CSVS'
files = ["raw_analyst_ratings.csv", "raw_partner_headlines.csv"]
analystCounter = 1
maxLimit = 1000
limitCounter = 0
for file in files:
    f = open(os.path.join(path, file),'r')
    csvfile = csv.reader(f)
    oldFields = next(csvfile)
    for row in csvfile:
        if(limit and limitCounter >= maxLimit):
            break
        sql = """SELECT * FROM StockInfo WHERE ticker=%s;"""
        cursor.execute(sql, (row[oldFields.index("stock")],))
        result = cursor.fetchall()
        if(len(result) < 1):
            continue

        analystRow = []
        analystRatingsRow = []
        analystID = 0
        for col in range(len(row)):
            if(oldFields[col] == "publisher"):
                if (any(row[col] in sublist for sublist in analystCSV)):
                    for sublist in analystCSV:
                        if row[col] in sublist:
                            analystID = analystCSV.index(sublist) + 1
                else:
                    # analystRow.append(analystCounter)
                    analystRow.append(row[col])
                    analystCSV.append((* analystRow,))
                    analystID = analystCounter
                    analystCounter+=1

            if(oldFields[col] in analystRatingsFields):
                analystRatingsRow.append(row[col])

        limitCounter+=1
        analystRatingsRow.insert(0, analystID)
        analystRatingsCSV.append((* analystRatingsRow,))
    f.close()

print("Inserting Into Analysts")
# for count in range(IPOInfoIDCounter - 1):
sql = """INSERT INTO Analysts
    (publisher)
    VALUES
    (%s);"""
try:
    cursor.executemany(sql, analystCSV)
    mydb.commit()
except (mysql.connector.Error, mysql.connector.Warning) as e:
    print(e)

print("Inserting Into Analyst Ratings")

sql = """INSERT INTO AnalystRatings
    (analystID, headline, ratingURL, ratingDate, ticker)
    VALUES
    (%s, %s, %s, %s, %s);"""
try:
    cursor.executemany(sql, analystRatingsCSV)
    mydb.commit()
except (mysql.connector.Error, mysql.connector.Warning) as e:
    print(e)


# filename = "Analysts.csv"

# with open(filename, 'w') as csvfile:
#     csvwriter = csv.writer(csvfile)

#     csvwriter.writerow(analystFields)

#     csvwriter.writerows(analystCSV)

# filename = "AnalystRatings.csv"

# with open(filename, 'w') as csvfile:
#     csvwriter = csv.writer(csvfile)

#     csvwriter.writerow(newAnalystRatingsFields)

#     csvwriter.writerows(analystRatingsCSV)
