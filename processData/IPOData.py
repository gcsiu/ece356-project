import os
import csv

IPOInfoFields = ["Symbol", "Name", "MarketCap", "Sector", "Industry", "City","stateCountry",  "Revenue", "netIncome", "employees", "YearFounded"]
newIPOInfoFields = ["dateIPO", "ticker", "fullName", "marketCap", "sector", "industry", "city","stateCountry", "revenue", "netIncome", "employees", "yearFounded"]

newIPOManagerFields = ["IPOInfoID", "jobTitle", "takeOverYear", "age", "managerName", "gender", "inChargeDuringIPO"]
IPOManagerCEOFields = ["CEOTakeOver", "CEOAge", "CEOName", "CEOGender", "CEOInChargeDuringIPO"]
IPOManagerPresidentFields = ["PresidentTakeOver", "PresidentAge", "PresidentName", "PresidentGender", "presidentInChargeDuringIPO"]

IPOTradingDataFields = ["dayClose", "dayHigh", "dayOpen", "dayLow", "dayVolume"]
newIPOTradingDataFields = ["IPOInfoID", "dayAfterIPO", "dayClose", "dayHigh", "dayOpen", "dayLow", "dayVolume"]

IPOInfoCSV = []
IPOManagerCSV = []
IPOAdvancedInfoCSV = []
IPOTradingDataCSV = []
oldFields = []
path = 'CSVS'
file = "IPODataFull.csv"
IPOInfoIDCounter = 1
f = open(os.path.join(path, file),'r', encoding='cp437')
# f = open(os.path.join(path, file),'r')
csvfile = csv.reader(f)
oldFields = next(csvfile)
maxLimit = 500
limitCounter = 0
for row in csvfile:
    if(limit and limitCounter >= maxLimit):
            break
    
    sql = """SELECT * FROM StockInfo WHERE ticker=%s;"""
    cursor.execute(sql, (row[oldFields.index("Symbol")],))
    result = cursor.fetchall()
    if(len(result) < 1):
        continue
    IPODate = str(row[oldFields.index("Year")]) + "-" + str(row[oldFields.index("Month")]) + "-" + str(row[oldFields.index("Day")])
    IPOInfoRow = [IPODate]
    IPOManagerCEORow = [IPOInfoIDCounter, "CEO"]
    IPOManagerPresidentRow = [IPOInfoIDCounter, "President"]
    IPOTradingDataRow = [IPOInfoIDCounter]
    # IPODayCounter = 0
    for col in range(len(row)):
        if(oldFields[col] in IPOInfoFields):
            if(row[col] == '' or row[col] == '-'):
                IPOInfoRow.append(0)
            else:
                IPOInfoRow.append(row[col])
        if(oldFields[col] in IPOManagerCEOFields): # 2 managers
            if(row[col] == '' or row[col] == '-'):
                IPOManagerCEORow.append(0)
            else:
                IPOManagerCEORow.append(row[col])
        if(oldFields[col] in IPOManagerPresidentFields): # 2 managers
            if(row[col] == '' or row[col] == '-'):
                IPOManagerPresidentRow.append(0)
            else:
                IPOManagerPresidentRow.append(row[col])
        
        # # IPO Trading Data Rows
        # if("@closeDay" in oldFields[col]):
        #     IPOTradingDataRow.append(IPODayCounter)
        #     IPOTradingDataRow.append(row[col])
        # if("@highDay" or "@openDay" or "@lowDay" in oldFields[col]):
        #     IPOTradingDataRow.append(row[col])
        # if("@volumeDay" in oldFields[col]):
        #     IPOTradingDataRow.append(row[col])
        #     IPOTradingDataCSV.append(IPOTradingDataRow)
        #     IPOTradingDataRow = [IPOInfoIDCounter]
        #     IPODayCounter+=1

    IPOInfoCSV.append((* IPOInfoRow,))
    IPOManagerCSV.append((* IPOManagerCEORow,))
    IPOManagerCSV.append((* IPOManagerPresidentRow,))
    IPOInfoIDCounter+=1
    limitCounter+=1
f.close()

# filename = "IPOInfo.csv"

# with open(filename, 'w') as csvfile:
#     csvwriter = csv.writer(csvfile)

#     csvwriter.writerow(newIPOInfoFields)

#     csvwriter.writerows(IPOInfoCSV)
print("Inserting Into IPOInfo")

# for count in range(IPOInfoIDCounter - 1):
sql = """INSERT INTO IPOInfo
    (dateIPO, ticker, fullName, marketCap, sector, industry, city, stateCountry, revenue, netIncome, employees, yearFounded)
    VALUES
    (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"""
try:
    cursor.executemany(sql, IPOInfoCSV)
    mydb.commit()
except (mysql.connector.Error, mysql.connector.Warning) as e:
    print(e)

print("Inserting Into IPOManager")
sql = """INSERT INTO IPOManager
    (IPOInfoID, jobTitle, takeOverYear, age, managerName, gender, inChargeDuringIPO)
    VALUES
    (%s, %s, %s, %s, %s, %s, %s);"""
try:
    cursor.executemany(sql, IPOManagerCSV)
    mydb.commit()
    # cursor.execute(sql, IPOManagerCSV[count*2 + 1])
    # mydb.commit()
except (mysql.connector.Error, mysql.connector.Warning) as e:
    print(e)


# print("Inserting Into IPOTradingData")
# sql = """INSERT INTO IPOTradingData
#     (IPOInfoID, dayAfterIPO, dayClose, dayHigh, dayOpen, dayLow, dayVolume)
#     VALUES
#     (%s, %s, %s, %s, %s, %s, %s);"""
# try:
#     cursor.executemany(sql, IPOTradingDataCSV)
#     mydb.commit()
# except (mysql.connector.Error, mysql.connector.Warning) as e:
#     print(e)

# filename = "IPOManager.csv"

# with open(filename, 'w') as csvfile:
#     csvwriter = csv.writer(csvfile)

#     csvwriter.writerow(newIPOManagerFields)

#     csvwriter.writerows(IPOManagerCSV)

# filename = "IPOTradingData.csv"

# with open(filename, 'w') as csvfile:
#     csvwriter = csv.writer(csvfile)

#     csvwriter.writerow(newIPOTradingDataFields)

#     csvwriter.writerows(IPOTradingDataCSV)
