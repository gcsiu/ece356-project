print("Loading all Stock Tickers into StockInfo")
# sql = "LOAD DATA INFILE '/var/lib/mysql-files/18-Stocks/all_symbols.txt' IGNORE INTO TABLE StockInfo;"
# cursor.execute(sql)
# mydb.commit()
import os
import csv
path = 'CSVS'
file = 'all_symbols.txt' 
f = open(os.path.join(path, file),'r')
csvfile = csv.reader(f)
newCSV = []
maxLimit = 25
limitCounter = 0
for row in csvfile:
    if(limit and limitCounter >= maxLimit):
        break
    newCSV.append((* row,))
    limitCounter+=1
f.close()

sql = """INSERT INTO StockInfo
    (ticker)
    VALUES
    (%s);"""
try:
    cursor.executemany(sql, newCSV)
    mydb.commit()
except (mysql.connector.Error, mysql.connector.Warning) as e:
    print(e)

print("Loading all trading data into Trading Data")
sql = "SELECT * FROM StockInfo;"
cursor.execute(sql)
result = cursor.fetchall()
counter = 0
maxLimit = 250
if result:
    for tuple in result:
        if(limit and counter > maxLimit):
            break
        sql = """LOAD DATA INFILE '/var/lib/mysql-files/18-Stocks/full_history/%s.csv' IGNORE INTO TABLE TradingData 
        FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 ROWS
        (date, volume, openPrice, high, low, closePrice, adjClose)
        SET ticker = '%s';""" % (tuple[0], tuple[0])
        try:
            cursor.execute(sql)
            mydb.commit()
        except (mysql.connector.Error, mysql.connector.Warning) as e:
            print(e)
        counter+=1

# import os
# import csv

# newFields = ["ticker", "date", "volume", "openPrice", "high", "lowPrice", "close", "adjClose"]
# newCSV = []
# tickers = []
# oldFields = []
# path = '/var/lib/mysql-files/18-Stocks/'
# files = os.listdir(path)

# for file in files:
#     if file.endswith("csv") and os.path.isfile(os.path.join(path, file)):
#         f = open(os.path.join(path, file),'r')
#         csvfile = csv.reader(f)
#         oldFields = next(csvfile)
#         tickerName = ((os.path.basename(file)).partition('.'))[0]
#         tickers.append([tickerName])
#         for row in csvfile:
#             newRow = row
#             newRow.insert(0, tickerName)
#             newCSV.append(newRow)
#         f.close()

# # ticker date, volume, open, high, low, close, adjClose
# filename = "stockInfo.csv"

# # with open(filename, 'w') as csvfile:
# #     csvwriter = csv.writer(csvfile)

# #     csvwriter.writerow(["ticker"])

# #     csvwriter.writerows(tickers)

# sql = "INSERT INTO StockInfo (ticker) VALUES (%s)"
# for ticker in tickers:
#     cursor.execute(sql, ticker)
#     mydb.commit()

# # filename = "tradingData.csv"

# # with open(filename, 'w') as csvfile:
# #     csvwriter = csv.writer(csvfile)

# #     csvwriter.writerow(newFields)

# #     csvwriter.writerows(newCSV)

# sql = "INSERT INTO StockInfo (ticker, date, volume, openPrice, high, low, closePrice, adjClose) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
# for row in newCSV:
#     cursor.execute(sql, row)
#     mydb.commit()
