import os
import csv

indicatorDataFields = ["EPS", "EBITDA", "Profit Margin", "Net Profit Margin", "priceEarningsRatio", "priceToSalesRatio", "priceCashFlowRatio", "priceEarningsToGrowthRatio", "grossProfitMargin", "cashRatio", "debtRatio"]
newIndicatorDataFields = ["ticker", "EPS", "profitMargin", "EBITDA", "netProfitMargin", "priceEarningsRatio", "priceToSalesRatio", "priceCashRatio", "priceEarningsToGrowthRatio", "grossProfitMargin", "cashRatio", "debtRatio"]

stockFinancialDataFields = ["Revenue", "Gross Profit", "Operating Expenses", "Operating Income", "Interest Expense", "Earnings before Tax", "Income Tax Expense", "Net Income", "Inventories", "Total assets", "Total debt", "Total liabilities",  "Total shareholders equity"]
newStockFinancialDataFields = ["ticker", "revenue", "grossProfit", "operatingExpenses", "operatingIncome", "interestExpense", "earningsBeforeTax", "incomeTaxExpense", "netIncome", "inventories", "totalAssets", "totalDebt", "totalLiabilities", "totalShareholdersEquity"]

indicatorDataCSV = []
stockFinancialDataCSV = []
oldFields = []
path = 'CSVS/FinancialData'
files = ["2014_Financial_Data.csv", "2015_Financial_Data.csv", "2016_Financial_Data.csv", "2017_Financial_Data.csv", "2018_Financial_Data.csv"]
maxLimit = 5000
limitCounter = 0
for file in files:
    f = open(os.path.join(path, file),'r')
    csvfile = csv.reader(f)
    oldFields = next(csvfile)
    for row in csvfile:
        if(limit and limitCounter >= maxLimit):
            break
        sql = """SELECT * FROM StockInfo WHERE ticker=%s;"""
        cursor.execute(sql, (row[0],))
        result = cursor.fetchall()
        if(len(result) < 1):
            continue

        indicatorDataRow = []
        stockFinancialDataRow = []
        analystID = 0
        for col in range(len(row)):
            if(oldFields[col] == ""):
                indicatorDataRow.append(row[col])
                stockFinancialDataRow.append(row[col])
            if(oldFields[col] in indicatorDataFields):
                if(row[col] == ''):
                    indicatorDataRow.append(0)
                else:
                    indicatorDataRow.append(row[col])
            if(oldFields[col] in stockFinancialDataFields):
                # if(limitCounter == 1):
                #     print(oldFields[col])
                if(row[col] == ''):
                    stockFinancialDataRow.append(0)
                else:
                    stockFinancialDataRow.append(row[col])


        indicatorDataCSV.append((* indicatorDataRow,))
        stockFinancialDataCSV.append((* stockFinancialDataRow,))
        limitCounter+=1
    f.close()

print("Inserting Into IndicatorData Table")
sql = """INSERT INTO IndicatorData
    (ticker, EPS, profitMargin, EBITDA , netProfitMargin, priceEarningsRatio, priceToSalesRatio, priceCashRatio, priceEarningsToGrowthRatio, grossProfitMargin, cashRatio, debtRatio)
    VALUES
    (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"""
try:
    cursor.executemany(sql, indicatorDataCSV)
    mydb.commit()
except (mysql.connector.Error, mysql.connector.Warning) as e:
    print(e)

print("Inserting Into StockFinancialData Table")

sql = """INSERT INTO StockFinancialData
    (ticker, revenue, grossProfit, operatingExpenses, operatingIncome, interestExpense, earningsBeforeTax, incomeTaxExpense, netIncome, inventories, totalAssets, totalDebt, totalLiabilities, totalShareholdersEquity)
    VALUES
    (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"""
try:
    cursor.executemany(sql, stockFinancialDataCSV)
    mydb.commit()
except (mysql.connector.Error, mysql.connector.Warning) as e:
    print(e)


# filename = "indicatorData.csv"

# with open(filename, 'w') as csvfile:
#     csvwriter = csv.writer(csvfile)

#     csvwriter.writerow(indicatorDataFields)

#     csvwriter.writerows(indicatorDataCSV)

# filename = "stockFinancialData.csv"

# with open(filename, 'w') as csvfile:
#     csvwriter = csv.writer(csvfile)

#     csvwriter.writerow(stockFinancialDataFields)

#     csvwriter.writerows(stockFinancialDataCSV)