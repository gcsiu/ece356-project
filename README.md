# F2021 ECE 356 Project - Stock Market

## Intial Data Loading & Processing
* Source the project.sql file on your mysql client
* Create a copy of **DatabaseCredentialsTemp.py** and rename it to **DatabaseCredentials.py**
* Fill out your database credentials in the DatabasesCredentials.py file that you just created
* Run preprocessing.py using python3 - a limit is set, so that the load times are not enormous - (Note: you might see some CSV files that do not exist as the tickers were in the all_symbols.txt but there was not a corresponding CSV file)
* If you want to load all of the data, set the limit variable to False in preprocessing.py
* Make sure that your table schema is sourced first

## Client Application
* Make sure you have completed the inital data loading and processing
* It is recommended to use stock tickers: "AAPL" or "A"
* To run the client app, run **python3 client.py** on the command line in the correct directory
* To navigate the client app, enter a number (corresponding with the option you want to select) and press Enter
* You can sign up for a user account and use that
* To manage users, you can use the admin account:
    * email: admin@marmoset.root
    * password: root