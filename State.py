class State(object):
    def __init__(self):
        print("Unimplemented State: Child states should present their text / menu prompts via overriding this initializer.")

    def on_event(self, event):
        pass

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return self.__class__.__name__