from States import WelcomeState

class StateMachine(object):
    def __init__(self):
        self.state = WelcomeState()

    def on_event(self, event):
        self.state = self.state.on_event(event)