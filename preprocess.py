import mysql.connector
import DatabaseCredentials
limit = True

credentials = DatabaseCredentials.DatabaseCredentials()
mydb = mysql.connector.connect(
  host=credentials.hostname,
  user=credentials.username,
  password=credentials.password,
  database=credentials.dbname,
  allow_local_infile=True
)

cursor = mydb.cursor()
err = mysql.connector.Error
# Stock tickers + trading data
print("Processing Stock Tickers and Trading Data")
exec(open("processData/tickerAndTradingData.py").read(), globals(), locals())

# IPO Tables
print("Processing IPO Tables")
# exec(open("processData/IPODataFullCreateLoad.py").read(), globals(), locals())
exec(open("processData/IPOData.py").read(), globals(), locals())

# Analyst Tables
print("Processing Analyst Ratings")
exec(open("processData/analystData.py").read(), globals(), locals())

# Indicator and Financial Data
print("Processing Indicator and Financial Data")
exec(open("processData/stockAndFinancialData.py").read(), globals(), locals())
