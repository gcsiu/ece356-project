DROP TABLE IF EXISTS IPOTradingData;
DROP TABLE IF EXISTS IPOManager;
DROP TABLE IF EXISTS IPOInfo;

DROP TABLE IF EXISTS StockFinancialData;
DROP TABLE IF EXISTS IndicatorData;

DROP TABLE IF EXISTS AnalystRatings;
DROP TABLE IF EXISTS Analysts;

DROP TABLE IF EXISTS TradingLogs;
DROP TABLE IF EXISTS Users;

DROP TABLE IF EXISTS TradingData;
DROP TABLE IF EXISTS StockInfo;

DROP TRIGGER IF EXISTS user_insert;

CREATE TABLE StockInfo (
    ticker VARCHAR(5) NOT NULL,

    PRIMARY KEY(ticker)
);

CREATE TABLE TradingData (
    tradingID INT NOT NULL AUTO_INCREMENT,
    ticker VARCHAR(5) NOT NULL,
    date DATE NOT NULL,
    volume INT NOT NULL,
    openPrice DECIMAL(18, 13) NOT NULL,
    high DECIMAL(18, 13) NOT NULL,
    low DECIMAL(18, 13) NOT NULL,
    closePrice DECIMAL(18, 13) NOT NULL,
    adjClose DECIMAL(19, 14) NOT NULL,
    
    PRIMARY KEY(tradingID),
    FOREIGN KEY(ticker) REFERENCES StockInfo(ticker)
);

CREATE TABLE IndicatorData (
    indicatorID INT NOT NULL AUTO_INCREMENT,
    ticker VARCHAR(5) NOT NULL,
    EPS DECIMAL(25, 15), -- 18
    EBITDA BIGINT, -- 28
    profitMargin DECIMAL(25, 15), -- 26
    netProfitMargin DECIMAL(25, 15), -- 32, 95
    priceEarningsRatio DECIMAL(20, 5), -- 80
    priceToSalesRatio DECIMAL(20, 5), -- 79
    priceCashRatio DECIMAL(20, 5), -- 81
    priceEarningsToGrowthRatio DECIMAL(25, 15), -- 84
    grossProfitMargin DECIMAL(20, 10), -- 92
    cashRatio DECIMAL(20, 10), -- 109
    debtRatio DECIMAL(25, 20), -- 115

    PRIMARY KEY(indicatorID),
    FOREIGN KEY(ticker) REFERENCES StockInfo(ticker)
);

CREATE TABLE Analysts (
    analystID INT NOT NULL AUTO_INCREMENT,
    publisher VARCHAR(255) NOT NULL,

    PRIMARY KEY(analystID)
);

CREATE TABLE AnalystRatings (
    ratingsID INT NOT NULL AUTO_INCREMENT,
    analystID INT NOT NULL,
    ticker VARCHAR(5) NOT NULL,
    headline TEXT NOT NULL,
    ratingUrl TEXT NOT NULL,
    ratingDate DATETIME NOT NULL,

    PRIMARY KEY(ratingsID),
    FOREIGN KEY(analystID) REFERENCES Analysts(analystID),
    FOREIGN KEY(ticker) REFERENCES StockInfo(ticker)
);

CREATE TABLE StockFinancialData (
    stockFinancialID INT NOT NULL AUTO_INCREMENT,
    ticker VARCHAR(5) NOT NULL,
    revenue BIGINT, -- 2
    operatingExpenses BIGINT, -- 8
    grossProfit BIGINT, -- 5
    operatingIncome BIGINT, -- 9
    interestExpense BIGINT, -- 10
    earningsBeforeTax BIGINT, -- 11
    incomeTaxExpense BIGINT, -- 12
    netIncome BIGINT, -- 15
    totalAssets BIGINT, -- 44
    totalDebt BIGINT, -- 49
    totalLiabilities BIGINT, -- 54
    inventories BIGINT, -- 37
    totalShareholdersEquity BIGINT, -- 57

    PRIMARY KEY(stockFinancialID),
    FOREIGN KEY(ticker) REFERENCES StockInfo(ticker)
);

CREATE TABLE IPOInfo (
    IPOID INT NOT NULL AUTO_INCREMENT,
    ticker VARCHAR(5) NOT NULL,
    fullName VARCHAR(255) NOT NULL, -- 1320
    sector VARCHAR(255), -- 1324
    industry VARCHAR(255), -- 1325
    city VARCHAR(255), -- 1338
    stateCountry VARCHAR(255), -- 1339
    dateIPO DATE NOT NULL, -- 1327
    revenue VARCHAR(10), -- 1341
    netIncome VARCHAR(10), -- 1342
    employees INT, -- 1344
    yearFounded INT, -- 1345
    marketCap BIGINT, -- 1322

    PRIMARY KEY(IPOID),
    FOREIGN KEY(ticker) REFERENCES StockInfo(ticker)
);

CREATE TABLE IPOManager (
    managerID INT NOT NULL AUTO_INCREMENT,
    IPOInfoID INT NOT NULL,
    managerName VARCHAR(255) NOT NULL,
    gender VARCHAR(15),
    age INT,
    jobTitle VARCHAR(255),
    inChargeDuringIPO VARCHAR(8),
    takeOverYear INT,

    PRIMARY KEY(managerID),
    FOREIGN KEY(IPOInfoID) REFERENCES IPOInfo(IPOID)
);

CREATE TABLE Users (
    userID INT NOT NULL AUTO_INCREMENT,
    type INT NOT NULL,
    fullName VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    pword VARCHAR(255) NOT NULL,

    PRIMARY KEY(userID)
);

CREATE TABLE TradingLogs (
    tradeID INT NOT NULL AUTO_INCREMENT,
    date DATE NOT NULL,
    userID INT NOT NULL,
    ticker VARCHAR(5) NOT NULL,
    type INT NOT NULL,
    quantity INT NOT NULL,
    price DECIMAL(10, 2) NOT NULL,

    PRIMARY KEY(tradeID, date),
    FOREIGN KEY(userID) REFERENCES Users(userID),
    FOREIGN KEY(ticker) REFERENCES StockInfo(ticker)
);

-- DELIMITER $$
-- CREATE TRIGGER user_insert 
-- BEFORE INSERT ON Users 
-- FOR EACH ROW
-- BEGIN
--     IF NEW.email REGEX '^[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,63}$' THEN 
--         call fail('EMAIL INVALID');
--     END IF;
-- END $$
-- DELIMITER ;

INSERT INTO Users (type, fullName, email, pword) VALUES (0, 'Admin', 'admin@marmoset.root', 'root');