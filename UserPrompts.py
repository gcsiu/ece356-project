enterOptionPrompt = "Enter option: "

welcomePrompt = """
--- ECE 356 Project ---
------- Welcome -------
Options:
   -1. Exit program
    1. Sign Up
    2. Log In
"""

signUpPrompt = "------- Sign Up -------"
signUpNamePrompt = "Please enter your full name: "
signUpEmailPrompt = "Please enter your email: "
signUpPasswordPrompt = "Please enter your password: "
signUpInvalidPrompt = "Unable to sign up. Returning to the main menu."

logInPrompt = "------- Log In -------"
logInEmailPrompt = "Please enter your email: "
logInPasswordPrompt = "Please enter your password: "
logInInvalidPrompt = "Unable to log in. Returning to the main menu."

mainMenuPrompt = """
\n------- Main Menu -------
Options:
   -1. Exit program
    0. Log Out
    1. Search a stock
    2. Search Analysts
    3. Search IPOs
    4. Issue a Trade
"""

adminMainMenuPrompt = """
------- Admin Main Menu -------
Options:
   -1. Exit program
    0. Log Out
    1. View all users
    2. Delete a user
"""

viewAllUsersTitle = "------- All Users -------"
deleteUserTitle = "------- Delete a User -------"
deleteUserPrompt = "Please enter the ID of the client you want to delete: "
deleteUserConfirmation = """Are you sure you want to delete user with ID %s?"
Options:
    0. No, don't delete
    1. Yes, delete
"""
noUsersPrompt = "No clients exist. Returning to the main menu."

searchStockTitle = "------- Search a Stock -------"
searchStockPrompt = "Please enter a stock ticker: "
searchStockOptions = """
Options:
    0. Return to the Main Menu
    1. Show indicator data
    2. Show financial data
    3. Show trading data
    4. Show IPO data
"""

indicatorDataTitle = "------- Indicator Data -------"
financialDataTitle = "------- Financial Data -------"
tradingDataTitle = "------- Trading Data -------"

searchAnalystsPrompt = "------- Search Analysts -------"
searchAnalystsNamePrompt = "Enter the name of the analyst: "
searchAnalystsNonePrompt = "No matching analysts found. Returning to the main menu."

analystsListPrompt = """
------- Search Results -------
Here are the top 10 search results for the analyst name that you've entered:
"""
analystsListPrompt2 = """
Options:
    0. Return to the Main Menu
    #. View all of the ratings by a particular analyst by entering the number next to the analyst's name
"""

analystRatingsPrompt = """
------- Ratings -------
Here are the first 10 ratings by the chosen analyst:
"""
analystRatingsPrompt2 = """
Options:
    0. Return to the Main Menu
    1. Search for another analyst
"""

searchIPOsPrompt = """
------- Search IPOs -------
Options:
    0. Return to the Main Menu
    1. Search by stock symbol
    2. View the 50 most recent IPOs
"""

searchIPOsBySymbolTitle = "------- Search IPOs by Symbol -------"
searchIPOsBySymbolPrompt = "Enter the stock symbol/ticker of the company you want to view IPO data for: "
searchIPOsBySymbolNonePrompt = "No matching companies found. Returning to the main menu."
IPOsListPrompt = """
------- Search Results -------
Here are the top 10 search results for the stock symbol/ticker that you've entered:
"""
IPOsListPrompt2 = """
Options:
    0. Return to the Main Menu
    #. View the IPO info for a particular company by entering the number next to the stock symbol/ticker
"""

recentIPOsListTitle = "------- 50 Most Recent IPOs -------"
recentIPOsListPrompt = """
Options:
    0. Return to the Main Menu
    #. View the IPO info for a particular company by entering the number next to the stock symbol/ticker
"""
IPOsNonePrompt = "No companies found. Returning to the main menu."

IPOInfoPrompt = "------- IPO Info -------"
IPOInfoPrompt2 = """
Options:
    0. Return to the Main Menu
    1. View info about this IPO's managers
"""

IPOManagersPrompt = "------- IPO Managers -------"
IPOManagersPrompt2 = """
Options:
    0. Return to the Main Menu
    1. Search for another IPO via stock symbol
    2. View the 50 most recent IPOs
"""
IPOManagersNonePrompt = "No IPO managers for this company found. Returning to the main menu."

issueTradePrompt = """
------- Issue Trade -------
Options:
    0. Return to the Main Menu
    1. Issue buy order
    2. Issue sell order
"""

buyStockTitle = "------- Issue Buy Order -------"
buySellStockPrompt = "Please enter a stock ticker: "
stockTickerDNEPrompt = "The stock ticker you have entered does not exist. Returning to the main menu."
buySellStockQuantityPrompt = "Please enter a quantity: "
stockTickerNoPricesPrompt = "No prices found. Returning to the main menu."
buyStockPrompt = "Your buy order was placed. Returning to the main menu."

sellStockTitle = "------- Issue Sell Order -------"
sellStockPrompt = "You have %s shares of %s stock."
sellStockQuantityPrompt = "Please enter a quantity to sell: "
sellStockFailedPrompt = "You do not have enough shares of %s stock."
sellStockCompletedPrompt = "Your sell order was completed. Returning to the main menu."

programCompletedPrompt = "Terminating program"