import DatabaseCredentials
import mysql.connector
import UserPrompts
import States
from StateMachine import StateMachine

credentials = DatabaseCredentials.DatabaseCredentials()
mydb = mysql.connector.connect(
  host=credentials.hostname,
  user=credentials.username,
  password=credentials.password,
  database=credentials.dbname,
  allow_local_infile=True
)

cursor = mydb.cursor()

stateMachine = StateMachine()

analystIDToSearch = -1
IPOSymbolToView = ""
IPOInfoIDToSearch = -1
currentUserID = -1

while True:
    option = input(UserPrompts.enterOptionPrompt)
    stateMachine.on_event(option)

    if isinstance(stateMachine.state, States.ProgramCompletedState):
        break

    if isinstance(stateMachine.state, States.SignUpState):
        name = input(UserPrompts.signUpNamePrompt)
        email = input(UserPrompts.signUpEmailPrompt)
        password = input(UserPrompts.signUpPasswordPrompt)

        # Ensure that all fields are 255 characters or less
        if len(name) > 255 or len(email) > 255 or len(password) > 255:
            print(UserPrompts.signUpInvalidPrompt)
            stateMachine.on_event(0)
            continue

        # Ensure that a user cannot sign up with an email that's already registered
        cursor.execute(f"SELECT * FROM Users WHERE email = '{email}'")
        result = cursor.fetchall()
        if result:
            print(UserPrompts.signUpInvalidPrompt)
            stateMachine.on_event(0)
            continue

        sql = "INSERT INTO Users (type, fullName, email, pword) VALUES (%s, %s, %s, %s)"
        values = (1, f"{name}", f"{email}", f"{password}")
        cursor.execute(sql, values)
        mydb.commit()

        stateMachine.on_event(1)

    if isinstance(stateMachine.state, States.LogInState):
        email = input(UserPrompts.logInEmailPrompt)
        password = input(UserPrompts.logInPasswordPrompt)

        cursor.execute(f"SELECT * FROM Users WHERE email = '{email}' AND pword = '{password}'")
        result = cursor.fetchall()

        # If a row from the Users table was fetched, then proceed to the main menu
        if result:
            currentUserID = int(result[0][0])
            if int(result[0][1]) == 1:
                stateMachine.on_event(1)
            else:
                stateMachine.on_event(2)
        else:
            print(UserPrompts.logInInvalidPrompt)
            stateMachine.on_event(0)

    if isinstance(stateMachine.state, States.LoggedInState):
        cursor.execute(f"SELECT DISTINCT ticker FROM TradingLogs WHERE userID = {currentUserID}")
        result1 = cursor.fetchall()

        processed = []
        for row in result1:
            ticker = str(row[0])
            cursor.execute(f"SELECT SUM(quantity) FROM TradingLogs WHERE ticker = '{ticker}' AND userID = {currentUserID} AND type = 0")
            loopRes1 = cursor.fetchall()
            totalBought = 0 if loopRes1[0][0] is None else loopRes1[0][0]

            cursor.execute(f"SELECT SUM(quantity) FROM TradingLogs WHERE ticker = '{ticker}' AND userID = {currentUserID} AND type = 1")
            loopRes2 = cursor.fetchall()
            totalSold = 0 if loopRes2[0][0] is None else loopRes2[0][0]

            processed.append([ticker, int(totalSold) - int(totalBought)])
        
        print("You own:")
        for stock in processed:
            print("Stock: %s, Quantity: %s" %(stock[0], stock[1]))
        print("\n")

    if isinstance(stateMachine.state, States.AdminViewAllUsersState):
        cursor.execute(f"SELECT userID, fullName, email, pword FROM Users WHERE type = 1")
        result = cursor.fetchall()
        result = [] if result[0][0] is None else result

        if len(result) != 0:
            print("userID | fullName | email | password")
            for row in result:
                print("%s | %s | %s | %s" %(row))
        else:
            print(UserPrompts.noUsersPrompt)
        stateMachine.on_event(0)

    if isinstance(stateMachine.state, States.AdminDeleteUserState):
        userid = input(UserPrompts.deleteUserPrompt)
        print(UserPrompts.deleteUserConfirmation %(userid))
        choice = input(UserPrompts.enterOptionPrompt)

        if int(choice):
            cursor.execute(f"DELETE FROM TradingLogs WHERE userID = '%s'" %(userid))
            cursor.execute(f"DELETE FROM Users WHERE userID = '%s' AND type = 1" %(userid))
            mydb.commit()
        stateMachine.on_event(0)

    if isinstance(stateMachine.state, States.SearchStockState):
        ticker = input(UserPrompts.searchStockPrompt)
        cursor.execute(f"SELECT ticker FROM StockInfo WHERE ticker = '{ticker}'")
        result = cursor.fetchall()

        if result:
            print(UserPrompts.searchStockOptions)
            option = input(UserPrompts.enterOptionPrompt)

            if int(option) != 0:
                IPOSymbolToView = result[0][0]
            stateMachine.on_event(option)
        else:
            print(UserPrompts.stockTickerDNEPrompt)
            stateMachine.on_event(0)

    if isinstance(stateMachine.state, States.TradingDataState):
        cursor.execute(f"SELECT ticker, date, volume, openPrice, high, low, closePrice, adjClose FROM TradingData WHERE ticker = '{IPOSymbolToView}' LIMIT 50")
        result = cursor.fetchall()
        result = [] if result[0][0] is None else result

        if len(result) != 0:
            print("Ticker | Date | Volume | Open | High | Low | Close | Adjusted Close")
            for row in result:
                print("%s | %s | %s | %s | %s | %s | %s | %s" % (row))

        stateMachine.on_event(0)

    if isinstance(stateMachine.state, States.FinancialDataState):
        cursor.execute(f"SELECT ticker, revenue, operatingExpenses, grossProfit, operatingIncome, interestExpense, earningsBeforeTax, incomeTaxExpense, netIncome, totalAssets, totalDebt, totalLiabilities, inventories, totalShareholdersEquity FROM StockFinancialData WHERE ticker = '{IPOSymbolToView}' LIMIT 1")
        result = cursor.fetchall()
        result = [] if result[0][0] is None else result

        if len(result) != 0:
            print("ticker | revenue | operatingExpenses | grossProfit | operatingIncome | interestExpense | earningsBeforeTax | incomeTaxExpense | netIncome | totalAssets | totalDebt | totalLiabilities | inventories | totalShareholdersEquity")
            for row in result:
                print("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s | %s | %s | %s | %s" % (row))

        stateMachine.on_event(0)
        

    if isinstance(stateMachine.state, States.IndicatorDataState):
        cursor.execute(f"SELECT ticker, EPS, EBITDA, profitMargin, netProfitMargin, priceEarningsRatio, priceToSalesRatio, priceCashRatio, priceEarningsToGrowthRatio, grossProfitMargin, cashRatio, debtRatio FROM IndicatorData WHERE ticker = '{IPOSymbolToView}' LIMIT 1")
        result = cursor.fetchall()
        result = [] if result[0][0] is None else result

        if len(result) != 0:
            print("ticker | EPS | EBITDA | profitMargin | netProfitMargin | priceEarningsRatio | priceToSalesRatio | priceCashRatio | priceEarningsToGrowthRatio | grossProfitMargin | cashRatio | debtRatio")
            for row in result:
                print("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s | %s | %s" % (row))

        stateMachine.on_event(0)

    if isinstance(stateMachine.state, States.SearchAnalystsState):
        analystName = input(UserPrompts.searchAnalystsNamePrompt)

        cursor.execute(f"SELECT * FROM Analysts WHERE publisher LIKE '%{analystName}%' LIMIT 10")
        result = cursor.fetchall()

        if result:
            print(UserPrompts.analystsListPrompt)
            # result is a list of tuples, so just print the publisher's name for each tuple in the list
            for i, analyst in enumerate(result):
                print(f"{i + 1}: {analyst[1]}")
            print(UserPrompts.analystsListPrompt2)

            option = input(UserPrompts.enterOptionPrompt)
            while int(option) > len(result):
                option = input(UserPrompts.enterOptionPrompt)
            if int(option) != 0:
                # Upon selecting an analyst, save the analystID so we can find the corresponding ratings in the next state
                analystIDToSearch = result[int(option) - 1][0]
            stateMachine.on_event(option)
        else:
            print(UserPrompts.searchAnalystsNonePrompt)
            stateMachine.on_event(0)

    if isinstance(stateMachine.state, States.AnalystRatingsState):
        cursor.execute(f"SELECT ticker, headline, ratingUrl, ratingDate FROM AnalystRatings WHERE analystID = {analystIDToSearch} LIMIT 10")
        result = cursor.fetchall()
        
        for i, rating in enumerate(result):
            print(f"{i + 1}: {rating[1]}\n   Symbol: {rating[0]}\n   URL: {rating[2]}\n   Date: {rating[3]}")
        print(UserPrompts.analystRatingsPrompt2)

    if isinstance(stateMachine.state, States.SearchIPOsBySymbolState):
        symbol = input(UserPrompts.searchIPOsBySymbolPrompt)

        cursor.execute(f"SELECT * FROM IPOInfo WHERE ticker LIKE '%{symbol}%' LIMIT 10")
        result = cursor.fetchall()

        if result:
            print(UserPrompts.IPOsListPrompt)
            for i, IPOInfo in enumerate(result):
                print(f"{i + 1}: {IPOInfo[1]}")
            print(UserPrompts.IPOsListPrompt2)

            option = input(UserPrompts.enterOptionPrompt)
            while int(option) > len(result):
                option = input(UserPrompts.enterOptionPrompt)
            if int(option) != 0:
                IPOSymbolToView = result[int(option) - 1][1]
            stateMachine.on_event(option)
        else:
            print(UserPrompts.searchIPOsBySymbolNonePrompt)
            stateMachine.on_event(0)

    if isinstance(stateMachine.state, States.ViewRecentIPOsState):
        cursor.execute(f"SELECT * FROM IPOInfo ORDER BY dateIPO LIMIT 50")
        result = cursor.fetchall()

        if result:
            for i, IPOInfo in enumerate(result):
                print(f"{i + 1}: {IPOInfo[1]}")
            print(UserPrompts.recentIPOsListPrompt)

            option = input(UserPrompts.enterOptionPrompt)
            while int(option) > len(result):
                option = input(UserPrompts.enterOptionPrompt)
            if int(option) != 0:
                IPOSymbolToView = result[int(option) - 1][1]
            stateMachine.on_event(option)
        else:
            print(UserPrompts.IPOsNonePrompt)
            stateMachine.on_event(0)

    if isinstance(stateMachine.state, States.IPODataState):
        cursor.execute(f"SELECT * FROM IPOInfo WHERE ticker = '{IPOSymbolToView}'")
        result = cursor.fetchall()

        if result:
            IPOInfo = result[0]
            print(f"{IPOInfo[1]}\nFull Name: {IPOInfo[2]}\nSector: {IPOInfo[3]}\nIndustry: {IPOInfo[4]}\nCity: {IPOInfo[5]}\nState Country: {IPOInfo[6]}\nDate IPO: {IPOInfo[7]}\nRevenue: {IPOInfo[8]}\nNet Income: {IPOInfo[9]}\nEmployees: {IPOInfo[10]}\nYear Founded: {IPOInfo[11]}\nMarket Cap: {IPOInfo[12]}")
            print(UserPrompts.IPOInfoPrompt2)

            option = input(UserPrompts.enterOptionPrompt)
            if int(option) != 0:
                IPOInfoIDToSearch = IPOInfo[0]
            stateMachine.on_event(option)
        else:
            print(UserPrompts.IPOsNonePrompt)
            stateMachine.on_event(0)

    if isinstance(stateMachine.state, States.ViewIPOManagersState):
        cursor.execute(f"SELECT * FROM IPOManager WHERE IPOInfoID = {IPOInfoIDToSearch}")
        result = cursor.fetchall()

        if result:
            for i, manager in enumerate(result):
                print(f"{i + 1}: {manager[2]}\n   Gender: {manager[3]}\n   Age: {manager[4]}\n   Job Title: {manager[5]}\n   In Charge during IPO: {manager[6]}\n   Takeover Year: {manager[7]}")
            print(UserPrompts.IPOManagersPrompt2)
        else:
            print(UserPrompts.IPOManagersNonePrompt)
            stateMachine.on_event(0)

    if isinstance(stateMachine.state, States.IssueTradeState):
        option = input(UserPrompts.enterOptionPrompt)
        stateMachine.on_event(option)

    if isinstance(stateMachine.state, States.BuyStockState):
        ticker = input(UserPrompts.buySellStockPrompt)
        cursor.execute(f"SELECT ticker FROM StockInfo WHERE ticker = '{ticker}'")
        result = cursor.fetchall()

        if result:
            cursor.execute(f"SELECT closePrice FROM TradingData WHERE ticker = '{ticker}' ORDER BY date DESC LIMIT 1")
            result = cursor.fetchall()

            if result:
                print(f"Price: {result[0][0]}")
                quantity = input(UserPrompts.buySellStockQuantityPrompt)
                sql = "INSERT INTO TradingLogs (date, userID, ticker, type, quantity, price) VALUES (NOW(), %s, %s, %s, %s, %s)"
                values = (f"{currentUserID}", f"{ticker}", 0, f"{quantity}", f"{result[0][0]}")
                cursor.execute(sql, values)
                mydb.commit()

                print(UserPrompts.buyStockPrompt)
                stateMachine.on_event(0)
            else:
                print(UserPrompts.stockTickerNoPricesPrompt)
                stateMachine.on_event(0)
        else:
            print(UserPrompts.stockTickerDNEPrompt)
            stateMachine.on_event(0)

    if isinstance(stateMachine.state, States.SellStockState):
        ticker = input(UserPrompts.buySellStockPrompt)
        cursor.execute(f"SELECT ticker FROM StockInfo WHERE ticker = '{ticker}'")
        result = cursor.fetchall()

        if result:
            cursor.execute(f"SELECT SUM(quantity) FROM TradingLogs WHERE ticker = '{ticker}' AND userID = {currentUserID} AND type = 0")
            result1 = cursor.fetchall()
            totalBought = 0 if result1[0][0] is None else result1[0][0]

            cursor.execute(f"SELECT SUM(quantity) FROM TradingLogs WHERE ticker = '{ticker}' AND userID = {currentUserID} AND type = 1")
            result2 = cursor.fetchall()
            totalSold = 0 if result2[0][0] is None else result2[0][0]

            remainingQuantity = int(totalBought) - int(totalSold)

            if remainingQuantity > 0:
                cursor.execute(f"SELECT closePrice FROM TradingData WHERE ticker = '{ticker}' ORDER BY date DESC LIMIT 1")
                currentPrice = cursor.fetchall()[0][0]

                print(UserPrompts.sellStockPrompt %(remainingQuantity, ticker))
                quantityToSell = remainingQuantity + 1

                while quantityToSell > remainingQuantity:
                    quantityToSell = int(input(UserPrompts.sellStockQuantityPrompt))
                    if quantityToSell > remainingQuantity:
                        print(UserPrompts.sellStockFailedPrompt)

                sql = "INSERT INTO TradingLogs (date, userID, ticker, type, quantity, price) VALUES (NOW(), %s, %s, %s, %s, %s)"
                values = (f"{currentUserID}", f"{ticker}", 1, f"{quantityToSell}", f"{currentPrice}")
                cursor.execute(sql, values)
                mydb.commit()

                print(UserPrompts.sellStockCompletedPrompt)
                stateMachine.on_event(0)
            else:
                print(UserPrompts.sellStockFailedPrompt %(ticker))
                stateMachine.on_event(0)
        else:
            print(UserPrompts.stockTickerDNEPrompt)
            stateMachine.on_event(0)