import UserPrompts
from State import State

# Signup / Login
class WelcomeState(State):
    def __init__(self):
        print(UserPrompts.welcomePrompt)

    def on_event(self, option):
        if int(option) == -1:
            return ProgramCompletedState()
        if int(option) == 1:
            return SignUpState()
        elif int(option) == 2:
            return LogInState()
        return self

class SignUpState(State):
    def __init__(self):
        print(UserPrompts.signUpPrompt)

    def on_event(self, option):
        if int(option) == 0:
            return WelcomeState()
        elif int(option) == 1:
            return LoggedInState()
        return self

class LogInState(State):
    def __init__(self):
        print(UserPrompts.logInPrompt)

    def on_event(self, option):
        if int(option) == 0:
            return WelcomeState()
        elif int(option) == 1:
            return LoggedInState()
        elif int(option) == 2:
            return AdminLoggedInState()
        return self

# Main Menu
class LoggedInState(State):
    def __init__(self):
        print(UserPrompts.mainMenuPrompt)

    def on_event(self, option):
        # TODO: Add transitions to remaining states
        if int(option) == -1:
            return ProgramCompletedState()
        elif int(option) == 0:
            return WelcomeState()
        elif int(option) == 1:
            return SearchStockState()
        elif int(option) == 2:
            return SearchAnalystsState()
        elif int(option) == 3:
            return SearchIPOsState()
        elif int(option) == 4:
            return IssueTradeState()
        return self

class AdminLoggedInState(State):
    def __init__(self):
        print(UserPrompts.adminMainMenuPrompt)

    def on_event(self, option):
        if int(option) == -1:
            return ProgramCompletedState()
        elif int(option) == 0:
            return WelcomeState()
        elif int(option) == 1:
            return AdminViewAllUsersState()
        elif int(option) == 2:
            return AdminDeleteUserState()
        return self

class AdminViewAllUsersState(State):
    def __init__(self):
        print(UserPrompts.viewAllUsersTitle)
    def on_event(self, option):
        return AdminLoggedInState()

class AdminDeleteUserState(State):
    def __init__(self):
        print(UserPrompts.deleteUserTitle)
    def on_event(self, option):
        return AdminLoggedInState()

# Search Stock
class SearchStockState(State):
    def __init__(self):
        print(UserPrompts.searchStockTitle)
    def on_event(self, option):
        if int(option) == 0:
            return LoggedInState()
        elif int(option) == 1:
            return IndicatorDataState()
        elif int(option) == 2:
            return FinancialDataState()
        elif int(option) == 3:
            return TradingDataState()
        elif int(option) == 4:
            return IPODataState()
        return self

class IndicatorDataState(State):
    def __init__(self):
        print(UserPrompts.indicatorDataTitle)
    def on_event(self, option):
        return SearchStockState()

class FinancialDataState(State):
    def __init__(self):
        print(UserPrompts.financialDataTitle)
    def on_event(self, option):
        return SearchStockState()

class TradingDataState(State):
    def __init__(self):
        print(UserPrompts.tradingDataTitle)
    def on_event(self, option):
        return SearchStockState()

class IPODataState(State):
    def __init__(self):
        print(UserPrompts.IPOInfoPrompt)
    def on_event(self, option):
        if int(option) == 0:
            return LoggedInState()
        elif int(option) == 1:
            return ViewIPOManagersState()
        return self

# Analysts
class SearchAnalystsState(State):
    def __init__(self):
        print(UserPrompts.searchAnalystsPrompt)

    def on_event(self, option):
        if int(option) == 0:
            return LoggedInState()
        return AnalystRatingsState()

class AnalystRatingsState(State):
    def __init__(self):
        print(UserPrompts.analystRatingsPrompt)

    def on_event(self, option):
        if int(option) == 0:
            return LoggedInState()
        elif int(option) == 1:
            return SearchAnalystsState()
        return self

# IPOs
class SearchIPOsState(State):
    def __init__(self):
        print(UserPrompts.searchIPOsPrompt)

    def on_event(self, option):
        # TODO: Add transitions to remaining states
        if int(option) == 0:
            return LoggedInState()
        elif int(option) == 1:
            return SearchIPOsBySymbolState()
        elif int(option) == 2:
            return ViewRecentIPOsState()
        return self

class SearchIPOsBySymbolState(State):
    def __init__(self):
        print(UserPrompts.searchIPOsBySymbolTitle)
    def on_event(self, option):
        if int(option) == 0:
            return LoggedInState()
        return IPODataState()

class ViewRecentIPOsState(State):
    def __init__(self):
        print(UserPrompts.recentIPOsListTitle)

    def on_event(self, option):
        if int(option) == 0:
            return LoggedInState()
        return IPODataState()

class ViewIPOManagersState(State):
    def __init__(self):
        print(UserPrompts.IPOManagersPrompt)

    def on_event(self, option):
        if int(option) == 0:
            return LoggedInState()
        elif int(option) == 1:
            return SearchIPOsBySymbolState()
        elif int(option) == 2:
            return ViewRecentIPOsState()
        return self

# Trade
class IssueTradeState(State):
    def __init__(self):
        print(UserPrompts.issueTradePrompt)

    def on_event(self, option):
        if int(option) == 0:
            return LoggedInState()
        elif int(option) == 1:
            return BuyStockState()
        elif int(option) == 2:
            return SellStockState()
        return self

class BuyStockState(State):
    def __init__(self):
        print(UserPrompts.buyStockTitle)

    def on_event(self, option):
        return LoggedInState()

class SellStockState(State):
    def __init__(self):
        print(UserPrompts.sellStockTitle)
    def on_event(self, option):
        return LoggedInState()

# Program Completed
class ProgramCompletedState(State):
    def __init__(self):
        print(UserPrompts.programCompletedPrompt)
    def on_event(self, option):
        return self